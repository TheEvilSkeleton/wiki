# Testers wanted

We need your help, either by running some tests on the NVIDIA binary driver or simply by using Nouveau on your main computer, updating it weekly and reporting any bugs and regressions you might encounter (see [[Bugs]] for more details on reporting bugs). 

This testing helps discovering regressions early which eases fixes. See [[InstallNouveau]] to know how to install Nouveau from source, or you can try one of the weekly built images available [[here|https://nouveau.pmoreau.org]].

# Outstanding requests

* Mesa/OpenGL tests: We're [[keeping track|http://people.freedesktop.org/~imirkin/]] of piglit test results (piglit is an OpenGL testsuite). If you don't see your card on there, please follow the instructions and send us the results.
