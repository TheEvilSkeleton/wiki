VP4 is the acceleration engine inside NVA3, NVA5, NVA8, NVAF, NVC0:NVD0 cards (see [[CodeNames]]). It corresponds to "VDPAU feature set C" in the NVIDIA documentation. It has full bitstream acceleration for MPEG1/2, H.264, VC-1, and MPEG4 Part 2. VP3 is the acceleration engine inside NV98, NVAA, and NVAC cards, which corresponds to "VDPAU feature set B".

For NVC0+ chips, there has been VDPAU support in mesa since version 9.1, and linux kernel 3.8.

In order to get pre-NVC0 chips to work, you need to

1. Extract firmware from the blob (see [[VideoAcceleration#firmware]]).
1. Install Mesa 10.0.1 or later.
1. Build kernel 3.12-rc1 or later, install, reboot
1. Run `mplayer -vo vdpau -vc ffmpeg12vdpau,ffvc1vdpau,ffh264vdpau,ffodivxvdpau, <file>`
