[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_17]]/ES/[[FR|Nouveau_Companion_17-fr]]/[[RU|Nouveau_Companion_17-ru]]/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 8 de Abril


### Introducción

La semana pasada no publiqué ninguna novedad ya que tenía la impresión de que cada uno de los temas tratados no se tomó en serio. Por lo que esperé otra semana más. Además, como marzo progresó lentamente también venía bien así. 

El fin de semana del 24 al 25 de marzo finalizó el periodo de solicitudes del Summer of Code de Google (SoC). Tuvimos unas 4-5 solicitudes, que están siendo evaluadas actualmente. Como no sabemos el número de plazas que tendremos en el meta proyecto Xorg para el SoC, no podemos decir cuántos proyectos saldrán adelante. Tenemos propuestas para: 

* nouveaufb: driver de framebuffer compatible con el driver para X acelerado de tarjetas nvidia (nouveau) 
* Soporte Xv para el driver 'Nouveau' 
* Soporte para texturado OpenGL para el driver 'Nouveau' 
* Incorporar soporte de Exa a más operaciones 
* Implementación de compresión S3TC libre de patentes para Mesa. Esta está dirigida a Mesa y se debe a una sugerencia de marcheu 
Por lo que tenemos 4 solicitudes de un total de 12 dirigidas a Xorg (según daniels). 

Respecto a nuestra idea del curso de introducción: probablemente lo haremos, pero no estamos seguros de cómo llevarlo a cabo. O bien lo escribiremos en una página del wiki y establecemos un periodo en el que la gente pueda hacer preguntas en el IRC, o bien comentaremos un tema en el IRC, dejaremos que se hagan preguntas a continuación, y luego lo escribiremos todo en el Wiki. En estos momentos nos inclinamos más por la opción #1, pero estad pendientes de las noticias que puedan aparecer aquí. 

Ahora el TiNDC se traduce al menos al español (ver #16), y tal vez también a otros idiomas. Han comenzados las traducciones del wiki al alemán, ruso y polaco. Gracias al equipo de traducción por el duro trabajo!. 


### Estado actual

Thunderbird y Skinkie trabajaron algo en la ingeniería inversa de los registros de salida de TV (TV-Out). Puesto que ninguno de ellos tienen demasiado tiempo, el avance es lento. Pero ahora Thunderbird es capaz de llevar a cabo cuestiones básicas como overscan, mover la imagen y cosas similares que son sencillas. Sin embargo, inicializar la salida de TV propiamente dicha es bastante complicado y no tiene una idea completa de cómo funciona. 

Ya tratamos sobre esto en el anterior número del TiNDC, pero en las semanas pasadas tuvimos algunas novedades: Está en marcha un nuevo gestor de memoria llamado TTM. Pero el hardware de NVidia es algo diferente, o avanzado, respecto al diseño actual y necesitamos asegurarnos de que nuestras necesidades están cubiertas. 

Una breve recapitulación sobre el hardware de NVidia: 

* Hasta 32 contextos de hardware en cada tarjeta (parece probable que la NV50 tenga 128) 
* Cambio de contexto realizado a través de un gestor de interrupciones (hasta las NV2x) o por la propia tarjeta (a partir de las NV3x). 
* Cada contexto cuenta con una cola FIFO y los objetos necesarios. La cola FIFO contiene todas las intrucciones, mientras que los 
objetos contienen la información necesaria para procesar la instrucción y almacenar los posibles resultados. 

* Los objetos se crean en función de la tarea que se va a procesar (p.e. 2D, 3D, procesado de Video) 
* Los objetos DMA (acceso directo a memoria) describen una región de la memoria y sus propiedades (tipo, dirección, límites disponibles y tipos de acceso permitidos) 
* Los contextos están totalmente separados entre sí. 
* Las colas FIFO se gestionan desde el espacio de usuario. 
Esto lleva a las siguientes exigencias del driver nouveau respecto al TTM: 

* cómo se supone que implementaremos el control de proceso de los buffer (fencing - marcas de seguimiento de las operaciones completadas) para que pueda usarse en múltiples corrientes de instrucciones separadas? 
* es preferible no implicar al kernel (DRM) con las colas FIFO 
* fencing / pinning (bloqueo de un buffer de memoria en una zona de memoria concreta) de múltiples buffer a la vez. 
* Puesto que el hardware ya realiza algas comprobaciones de validación, solamente debemos asegurarnos de que los buffer y sus direcciones son válidas. 
* El TTM es capaz de mover los buffer, e incluso eliminarlos completamente de la AGP/VRAM. 
Bien. totte (Thomas Hellström, un hacker del drm) se pasó por el canal #nouveau y debatió exactamente ese tema con wallbraker, marcheu y darktama. El resultado fue que necesitaríamos una nueva llamada ioctl() (control de E/S) en el drm que permita la validación y el control de proceso de buffers en el TTM. Este ioctl() solamente necesita ser llamado cuando se introduce una nueva textura desde la aplicación OpenGL, aunque esto puede no ser totalmente cierto, ya que darktama no está del todo seguro de que pueda funcionar. 

Actualmente creemos que necesitaríamos usar el TTM así: 1. Bloquear la dirección de todos los buffer en los que se está leyendo/escribiendo (llamado "validación" por el TTM) 2. Enviar las instrucciones que usan los buffer 3. Emitir puntos de control (fences) para que el TTM sepa cuando es segura la limpieza de datos (eviction). 

"Pinning" significa indicar al TTM que no puede mover o limpiar los buffer porque están siendo usados. Un "fence" (marcador) es fundamentalmente un simple contador. Tras enviar las instrucciones se indica al TTM que "cuando el marcador alcance este valor, ya no se usan esos buffer". Cuando se indica al TTM que valide los buffer, los marca como en uso, y tu luego asocias un objeto "fence" con ellos, y una vez que se señala el objeto fence (el contador de fence alcanza el valor deseado), el TTM marca el buffer como no usado. 

Stillunknown se hartó de que su NV43 (que es del mismo tipo que la de KoalaBR) todavía no funcionase, mientras que la de KoalaBR sí lo hacía, por lo que las 3 últimas semanas estuvo presionando a darktama para ayudarle a conseguir que su tarjeta funcionase. Finalmente, Darktama se rindió y le pidió las bitácoras del kernel con la salida de depuración del DRM, registros de actividad de las X  y las trazas de salida de la cola fifo de nouveau. Con estos datos Darktama pudo escribir un parche a medida. Stillunknown se encontró con un error en el swtcl (transformación, recorte e iluminación por software) que el resto de las tarjetas NV4x no encontraban al no usar ese tramo de código. [[Fuentes|http://gitweb.freedesktop.org/?p=mesa/mesa.git;a=commit;h=ea3d11a3d8901d650eb2a858ce30abae2d20d278]] Otro informe exitoso provino de mg, que hizo funcionar una NV44, a falta de las "palabras mágicas" para el cambio de contexto (context voodoo). 

PQ continuó su trabajo en la base de datos en xml de descripción de registros. Afinó el diseño con z3ro, para que sea útil para Radeon y Nouveau. Sin embargo, casi se vuelve loco cuando darktama mencionó casualmente que existen registros autoincrementados cuando se utilizan para escribir a otros registros (como cuando se carga el context voodoo). Casi perdemos a PQ en ese momento, pero rápidamente se recuperó cuando le vino la inspiración y encontró una forma sencilla de codificar esa información en su esquema de xml. Mientras que inicialmente se pensaba en simplemente mejorar la salida de mmio-parse ahora parece que nvclock (a largo plazo) y radeon pueden acabar usando también esta herramienta. 

abotezatu (que hizo una solicitud para el SoC) investigó los problemas restantes que impedían el cambio de contexto en las NV1x. 

Airlied continúa lentamente su trabajo en randr12 a causas de sus otras obligaciones. Su rama de desarrollo probablemente no funciona salvo para su configuración. Pero, puesto que trabaja igualmente sobre PPC y x86, ambas arquitecturas deberían estar igualmente rotas. Parece que será necesario interpretar algunas tablas de la BIOS para obtener los datos suficientes para que esto funcione. Por ahora, la interpretación de la tabla de las NV4x parece funcionar (las tarjetas anteriores probablmente no tengan esta tabla), pero darktama rompió la compatibilidad binaria del drm, por lo que se hizo necesaria una nueva reconciliación de cambios con la rama principal. Pero en estos momentos Airlied está trabajando en pulir los drivers  de Intel y el TTM. 

jb17some continúa su investigación para conocer porqué no es posible encontrar objetos en la memoria de instancia en tarjetas G70 y superiores. Tiene una mejor comprensión de porqué ocurren los bloqueos y en qué lugar se almacena la memoria de instancia. Parece que las tarjetas a partir de la G70 únicamente almacenan la memoria de instancia en el frame buffer y no en la región 0 de pci de 16mb. Esta teoría se verificó en sus su 7900gs (volcando la región 0 del pci + el frame buffer), comparando estos resultados con otros del almacén de volcados. Posteriores comprobaciones en una 6150 parecen corroborar que esta teoría es cierta para todas las tarjetas>= NV4x que usan memoria compartida. 

KoalaBR publicó el script para descargar, compilar y ejecutar renouveau de forma automática, además de crear un archivo adecuado con los datos de salida. Se puede encontrar un enlace al mismo en la página del Wiki de REnouveau Wiki y, actualmente, aquí: [[http://www.ping.de/sites/koala/script/createdump.sh|http://www.ping.de/sites/koala/script/createdump.sh]] 

Y antes de pedir de nuevo vuestra ayuda, tenemos un lote de sorpresas (bueno, no son sorpresas, pero esperamos que os gusten) 

* Marcheu arregló el cambio de contexto en NV04. Glxgears todavía no funciona ya que todavía faltan algunas correcciones de errores en la inicialización de 3d. 
* Julienr y Marcheu arreglaron los bloqueos en la Quadro FX 350M (PCIE) (ver error #10404 en Bugzilla). Si tienes bloqueos en chips Go7300/7400 o similares, deberías volver a probar e informarnos de qué ha ocurrido. 
* El cambio de contexto parece funcionar ahora en todas las tarjetas, pero no en todas ellas funciona glxgears. En algunas tarjetas se cuelgan al ejecutar algunas intrucciones 3d de DMA o FIFO. La razón de ello está siendo investigada en estos momentos. 

### Ayuda necesaria

Nos interesan los volcados de SLI y de Go7300/7400 (por favor, pon en el asunto del correo que se trata de un volcado SLI). También son muy bienvenidos los poseedores de una G80 que quieran probar parches. 

Y, sí, los volcados de [[MmioTrace|MmioTrace]] son igualmente bienvenidos. 

Los propietarios de Go7300/7400 deberían probar si nouveau (2d) funciona. 

Así que esta fué la edición de Pascua del TiNDC. Unos 3 días de trabajo que abarcan 3 semanas de desarrollo y que se lee en (más o menos) ¿3 minutos?. :) No importa. Espero que hayas disfrutado de su lectura. 
