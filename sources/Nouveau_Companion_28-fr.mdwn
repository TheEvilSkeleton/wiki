[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_28]]/[[ES|Nouveau_Companion_28-es]]/[[FR|Nouveau_Companion_28-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 14 Octobre 2007


### Introduction

Nous revoilà pour l'édition 28 sur Nouveau, projet de pilotes libres pour les cartes Nvidia. 

Non, nous ne travaillons pas tous seuls dans notre coin pour la joie d'écrire un pilote, nous donnons et recevons de l'aide d'autres projets. Haiku pour la réception ou MMioTrace pour le don en sont quelques exemples ; et, ces 2 dernières semaines, il semble que nous en ayons vu un nouveau éclore : le support pour la partie graphique des PS3 

Tout à commencer quand [[IronPeter|IronPeter]] est venu sur notre canal pour demander si nous avions des connaissances concernant le développement sur PS3 et plus spécifiquement comment gérer la carte Nvidia. Nous lui avons fournis quelques informations (mais également admis que nous ne serions pas d'une grande aide pour la partie spécifique aux PS3, aucun des développeurs n'en possédant une) et durant les jours qui suivirent, il revint poser des question sur le fonctionnement des cartes NVidia, questions auxquelles nous avons bien volontiers répondu. 

Pas mal de travail après, il a finalement réussi à obtenir un blitting 2D fonctionnel, éliminant ainsi une limitation importante de Linux sur PS3. Mais voyez par vous même : [[http://forums.ps2dev.org/viewtopic.php?t=8364|http://forums.ps2dev.org/viewtopic.php?t=8364]] Tous le travail a été fait par [[IronPeter|IronPeter]], nous lui avons simplement fourni quelques détails techniques. 

En lisant cette édition, vous remarquerez probablement qu'elle évoque assez peu la partie 3D du pilote. Cela est principalement du au fait qu'il faut que nous prenions en main et utilisions le TTM (qui devrait être aussi bon pour nous qu'il est possible de l'être, comme nous l'avons vu dans l'édition précédente). Un deuxième problème est la nouvelle architecture de pilote « Gallium3D ». 

Celle-ci dépend fortement de fonctionnalités récentes (telles que les shaders, les vertex programmables, etc) et essaye d'alléger la tâche des auteurs de pilotes en mutualisant le maximum d'infrastructure dans MESA. Bien sur, les anciens et nouveaux pilotes seront très différents. Gallium3D est encore très récent et il y aura sûrement quelques révisions avant d'avoir une API stable. De plus, Gallium3D dépendant de fonctions assez nouvelles, écrire un pilote pour une carte ancienne ne sera pas simple. 

Nous sommes donc encore une fois en attente. Mais cette fois-ci, nous avons un plan : obtenir une 2D fonctionnelle pour toutes les cartes jusqu'au NV4x et ensuite, pourquoi pas faire une première version. A prendre avec des précautions bien sur, l'API binaire sera peut-être encore cassée par la suite. 


### Statut actuel

Revenons à notre cœur de projet, où nous avons vu pas mal de progrès également. Pour l'instant nous visons un driver 2D qui marche (sans EXA) pour tous les modèles depuis le NV04 jusqu'au NV4x. Malheureusement, le NV5x est une autre affaire et ça va prendre plus de temps. Et même, depuis le 05/10/2007 notre branche principale ne marchait plus du tout pour les NV5x / G8x. Mais après quelques test et bisection par KoalaBR, stillunknown a découvert et réparé une erreur ce qui a permis d'avoir la 2D qui marche. Quelque chose de gênant quand même, revenir en mode texte bloque totalement l'affichage, ce qui rend difficile le test du drivers en cas d'erreurs. 

Sur NV04, ahuillet a pris par à une difficile bataille afin de faire marcher X. Ses premiers essais se sont déroulés dans l'indifférence la plus totale de la part de la carte. Et alors qu'ahuillet voulait abandonner, matc lui a montré qu'il faisait quelques fausses suppositions. Si on ajoute le fait qu'il avait un problème matériel sur sa carte mère, cela résultait en des problèmes d'initialisation aléatoires sur la NV04 ainsi que sur une NV11.  Donc ahuillet a mis en pièces son code, a réessayé et a finalement progressé : Ses méthodes logicielles étaient appelées par la carte, des commandes invalides généraient les interruptions d'erreur attendues. La tâche suivante est l'implémentation de quelques méthodes logicielles. Des méthodes logicielles sont des commandes qui, lorsqu'elles sont envoyées à la carte, lui fait demander à DRM de les gérer. NV04 gère quelques méthodes d'initialisation de manière logicielle, peut-être à cause de décisions lors de la conception. Ces méthodes modifient juste un flag dans des instances d'objets, et sont donc simples à écrire.  Quand une méthode logicielle doit être gérée, la carte génère une interruption spécifique. 

Il a placé son code dans son propre dépôt git et attend que ça soit remonté sur le dépôt principal « bientôt » (dans les prochains jours). Cependant, ahuillet doit d'abord résoudre un dernier problème : X démarre, mais patauge dans la mélasse puisque la carte génère une tempête d'interruptions de type STATE_INVALID. Cela pourrait avoir quelque chose à voir avec la gestion des interruptions (il y a eu un petit changement par ahuillet et darktama), mais il y a plus de chances que ça soit quelque chose d'autre (peut-être qu'il faut envoyer un accusé de réception pour l'état). Des investigations sont en cours. Et avant d'oublier : l'overlay marche aussi sur NV04. 

Pmdata a essayé de venir à bout des problèmes de rendu de glxgears avec des tailles de fenêtres autres que celles par défaut. Ce sujet est pour le moment en pause en raison de son travail. 

Pq, quand à lui, a essayé de faire fonctionner NV20-demo ([[http://cgit.freedesktop.org/~pq/nv20_demo/|http://cgit.freedesktop.org/~pq/nv20_demo/]]). Il a utilisé MMiotrace et commencé à analyser le dump résultant. Pour faciliter le debogage, il a commencé à écrire un rejoueur de mmio-trace. Cet outil utilise un dump MMio et le rejoue ligne par ligne. Ainsi, on peut voir quel effet est associé à quelle écriture dans un registre. Associé à quelques essais/erreurs, cela a permis de réduire la taille du dump de 60Mo à 4500 lignes. Et ce n'est pas simple parce que chaque rejeu nécessite un redémarrage, et comme certains états de la carte survivent à une hors tension de moins de 30 secondes, cela prend un peu de temps. Au final, Marcheu s'est rappelé pourquoi ce problème existait : il n'y a pas d'initialisation des contextes PGRAPH dans le DRM, personne ne les ayant écrit pour le moment. 

Après quelques test, il obtint une initialisation et un rendu de triangles fonctionnels. La prochaine étape était glxgears. Pour aller vite, pq a patché Mesa pour lui faire croire que sa NV28 était une NV1x, ce qui a permis de fonctionner, dans les limites des NV1x (voire les éditions précédentes). Ce patch n'a jamais été commité vu que c'est un hack et qu'il y a besoin d'une implémentation propre dans le pilote. Néanmoins, l'initialisation signifie une initialisation correcte par contexte, laquelle nécessite de gros changement structurels dans le DRM. En dehors des NV28/25, toutes les autres NV2x se sont retrouvées non fonctionnelles. A ce moment, malc0_ fournit un patch pour améliorer l'initialisation des NV2x : [[http://people.pwf.cam.ac.uk/sb476/nouveau/nv20pgraphctx/|http://people.pwf.cam.ac.uk/sb476/nouveau/nv20pgraphctx/]] Celui-ci permit de retrouver un fonctionnement pour toutes les NV2x. Un autre résultat de la combinaison de tous ces patchs est que les NV2x font maintenant le changement de contexte automatiquement et n'ont plus besoin d'interception manuelle. 

Plus d'information sur les contextes NV2x et l'initialisation ici : [[http://nouveau.freedesktop.org/wiki/Nv20GraphInit|http://nouveau.freedesktop.org/wiki/Nv20GraphInit]] 

Le travail sur NV20 s'est révélé plus difficile que prévu, pq obtenant toujours un blocage de la queue DMA au début du démarrage de X 

Discutant de ce problème avec Marcheu, celui-ci lui demanda de forcer le stockage des notificateurs dans la mémoire de la carte au lieu de la mémoire AGP. Ceci fonctionna et prouva que pq avaient des problèmes avec l'AGP : les notificateurs placés dans cette partie de la mémoire ne fonctionnent pas (ce qui est le cas pour certaines cartes mères). (Pour plus de détail sur les notificateurs, référez vous aux 2 derniers TiNDC). 

Marcheu et Jkolb ont beaucoup travaillé pour faire fonctionner les NV3x. Dans un premier temps, ils ont essayé de faire fonctionner nv30_demo (qui rend des triangles colorés, texturés ou simples) ; ce qui a été rendu difficile par des erreurs d'initialisation. Après avoir recopié intégralement le code d'initialisation de nouveau, il s'est mis à fonctionner mais crash avec l'erreur INVALID_COMMAND après l'envoi au FIFO et l'exécution de quelques commandes. 

Quelques bogues furent trouvés, éradiqués et jkolb parvint au niveau suivant : un triangle rendu. Mais toujours noir (et comme le fond était noir aussi, le premier diagnostic fut : « il ne rend rien du tout » :) ). L'étape suivante fut réalisé par Marcheu : un triangle coloré. Pour ne pas être en reste, jkolb ajouta le texturing. Par la suite, ils cherchèrent à faire fonctionner EXA sur NV30. Là encore, marcheu réussi avec l'aide de jkolb, mais que marcheu testa les performances avec un gestionnaire de composition, et compara celle de "[[MigrationHeuristics|MigrationHeuristics]] Always" (devrait être plus rapide) versus "[[MigrationHeuristics|MigrationHeuristics]] Greedy" (plus rapide quand EXA n'est pas complètement accéléré), il trouva que le premier était bien plus lent que le second. Et il décidèrent d'enquêter plus avant sur le sujet. L'accélération complète EXA sur NV3x n'est pour l'instant donc pas très utilisable en raison de mauvaises performance et de routines manquantes (plus quelques bogues). 

Quelques nouvelles rapidement : 

* Certaines routines memcpy() ont été alignées, ce qui apporte quelques gains de vitesse importants 
* Encore une fois, des bogues d'endianness sur PPC ont été investigués par Marcheu. Ils se sont révélés assez difficiles à détecter mais avec l'aide de BenH, Marcheu a pu les corriger. Malheureusement le PowerBook du rapporteur a vu son disque dur crashé, et nous ne sommes donc pas sûr que la correction fonctionne. Pire encore, il semble qu'une option de compilation spécifique aux x86/x86-64 s'est glissée dans le makefile (pour l'alignement ci-dessus). 
* De façon à rendre le pilote NV5x plus facile à maintenir, GrowlZ s'est chargé de désobscurcir les parties « volées » au pilote nv. Il avait déjà été légèrement désobscurci par darktama pour les parties adaptées au DRM. 
* pq a travaillé sur la génération de documentation à partir de rules-ng. le résultat est disponible ici : [[http://people.freedesktop.org/~pq/rules-ng/|http://people.freedesktop.org/~pq/rules-ng/]] Quand ahuillet a vu ça, il a promis d'écrire également une documentation pour les registres Xv. 
Bien, que manque t'il encore pour avoir un pilote 2D acceptable : 

* - NV10 : EXA doit être implémenté - NV04 : la tempête d'interruption doit être calmé - NV20 : utilisera EXA de NV10 - NV30 : EXA doit être plus rapide (et les fallback doivent disparaître) - NV40 : EXA fonctionne mais pas aussi rapidement qu'il pourrait - NV50 : gérer le mode texte (pas de XV encore) 

### Aide requise

Ahuillet est à la recherche de testeur pour le code NV04. Et si quelqu'un veut écrire de la documentation, contactez le également. Il vous donnera toutes les informations dont vous avez besoin, vu qu'il n'a pas le temps de l'écrire lui-même actuellement (une tâche junior intéressante). 

Si vous avez une machine PPC, testez nouveau et informez Marcheu du résultat. 


### Merci

Encore une fois, nous avons reçu des donations de matériel. De la part du projet Nouveau, Pekka Paalanen (pq) aimerait remercier Ville Herva de Vianova Systems Finland Oy pour lui avoir fait don une Geforce 3 et une Geforce 256. Merci beaucoup ! 

[[<<< Édition précédente|Nouveau_Companion_27-fr]] | [[Édition suivante >>>|Nouveau_Companion_29-fr]] 
