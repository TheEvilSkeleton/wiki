# FAQ - Frequently asked questions

Nouveau troubleshooting answers are now on their own page: [[TroubleShooting|TroubleShooting]] 

[[!toc startlevel=2 levels=2]] 


## General Questions


### I want to donate to your project...

You can help us by donating your time. Documentation, web page, reverse engineering or coding: all offer vacancies. We currently don’t accept money. We do accept hardware, however (especially newer NVIDIA cards are needed to improve support for them): see below. 


### What about the pledge at PledgeBank?

That [[pledge|http://www.pledgebank.com/nouveaudriver]] was set up without us knowing and was in no way initiated by us. When we heard about it, we honestly didn’t expect it to succeed. Well, it seems we were wrong. Currently (10.01.2007) the initiator of the pledge is working out the payment details and will let us know how to proceed. Please have a look at the pledge page and his personal blog for the latest information about that. 

_Update 6th Dec 2008:_ The pledge still has not worked out, because we cannot find an organization willing to take care of the money transfer. Due to tax and legal issues, it is not worth the trouble and costs to accept money without a proper organization handling the paperwork. 


### When can we expect a working driver ?

It really is not easy to predict dates, as show-stoppers may appear any time. Very few, usually none, of the developers are paid to work on Nouveau, so progress depends on their free time and interests. 


### What is the current status of the driver?

See the [[FeatureMatrix]]. 


### I want to help! What can I do?

The most helpful thing is to become a developer! We need more developers. Practically nothing says _"I love you, developers"_ better than sending a nice, clean patch that fixes a real problem. Otherwise, the easiest thing is probably to help with testing and reverse engineering.  See the [[front page|index]]. 


### What is an easy route to becoming a developer?

First you should have some programming skills with the C language. That's pretty much the only requirement, everything else you can learn on the way. Knowing something about graphics or hardware is a big plus. 

Developers usually starts by finding something you think should be fixed, in the driver or the reverse engineering tools. When you find that thing, ask people about it, read the relevant code, and figure stuff out. A good way to find your own developer’s itch is to just start using Nouveau, hang out on the IRC channel, and maybe subscribe to some mailing lists. When you find a good itch, you have to scratch &mdash; this is what drives development. And when you scratch, the itch usually spreads ;-) 

Our [[Introductory Course|IntroductoryCourse]] is a good first read. 


### Do you get any support from NVidia?

We didn’t get any support from NVidia for a long time.  We started our code from the [[nv Xorg driver|http://xorg.freedesktop.org/wiki/nv]]. That driver used to be maintained by NVidia and supports all cards up to NVC0, but only for 2-D and nv’s 2D acceleration is very slow compared to nouveau’s. See “[[How did you find out how NVidia cards work, then?|FAQ]]”.

However since September 2013, NVidia started releasing [[register and class definitions|https://github.com/NVIDIA/open-gpu-doc]] and answering some of our questions. Beside that they started adding support in Nouveau for the GK20A chipset.


### Why are you doing this?

We can’t give you _the answer_, as each project member has his own motivation. Just a few answers from our staff we got when this question was raised: 

* Don't like binary blobs 
* Want to give back to the OSS community 
* Want to learn driver programming 
* Yes, we can develop our own drivers regardless of what people at NVidia may think 
* Support for missing features 
* Support for operating systems not supported by NVidia (any PowerPC based OS for example) 
* Just for the fun of it 
* Binary driver keeps crashing even in 2D 
* Slow Xorg "nv" driver (slow in performance and slow to get new card support) 

So pick the reasons you feel are important; chances are that quite a few project members will agree with your pick :) 


### Hardware donations

While many of us already have enough hardware to keep us busy for weeks, we are usually interested in hardware donations, even of old cards. If you have a card to donate, please write to the Nouveau mailing list (see [[About Nouveau|index]]) and tell what card you have. The people involved in nouveau development will try to answer you as soon as possible. 


## Technical questions

<a name="Methods"></a> 
### How did you find out how NVidia cards work, then?

Basically, we store the state of the card before and after running a simplistic OpenGL program. After that, we `diff` the states in order to find out what was sent to the card. See the [[REnouveau|REnouveau]] documentation for further information. We also look at how the proprietary kernel driver accesses the card registers by intercepting the accesses using [[MmioTrace|MmioTrace]]. There are also other tools to watch the card. 


### Why don’t you just disassemble the proprietary driver?

In some notable countries that would be illegal, putting the project and its users in jeopardy. 


### So go to a country where it’s not illegal and produce specs!

OK, the technical answer: The Nouveau project needs to know how to drive the hardware. We can find that out without disassembling the proprietary driver (the blob). We are **not** trying to duplicate the design of the blob, because there is an existing [[direct rendering infrastructure|http://dri.freedesktop.org/]] we are using. 

The blob has its own kernel interfaces, which are not of such quality that they will likely ever be part of the mainline kernel &mdash; they are specific to the blob and don’t care about the DRI/DRM infrastructure. We also have [[Mesa|http://www.mesa3d.org]] and [[Gallium|http://wiki.freedesktop.org/wiki/Software/gallium]] to provide the OpenGL interfaces; the blob has its own version of everything. We really don’t want to try to understand or even see all the kludges and hacks that might exist in the blob. Disassembling the blob would be a great effort and produce a lot of useless information, and it could still become a legal problem. 

In short: disassembling is a lot more trouble than it’s worth. 

Many developers are already in countries where it is not straightforwardly illegal to disassemble the blob. However, none of the developers are lawyers, so we prefer to stay well clear from any potential legal issues. 


### Do you support dual-head systems / Randr 1.2?

Yes, Randr 1.2 is supported (and cannot be disabled). 


### Which cards are you trying to support?

Everything starting from NV04. Older cards may see some support when everything is working and the developers have time to do this. 


### Does my card work with Nouveau?

The most likely answer for your particular Nvidia graphics card is: yes for 2D and maybe for 3D. You can find the details in [[Status|index]] on the front page. 

If you have a laptop with dual Intel/Nvidia graphics, the Nvidia card may or may not be usable. The card may be selectable via vgaswitcheroo or a BIOS option, you should check those first. If neither works, you are probably out of luck. See the above link for Nvidia Optimus. 

If you want a definite answer, try it. 


### Which Xorg version do I need?

See [[InstallNouveau|InstallNouveau]]. 


### Which systems do you support?

The developers are currently working on Linux, where Nouveau runs on AMD64, x86 and PowerPC architectures. There used to be some FreeBSD support in UMS days (see [[Nouveau Companion 43|Nouveau Companion 43]]), but Nouveau is KMS-only now. 


### Is hibernation supported?

Suspend to RAM works, and so should suspend to disk. 


### Will you support PS3 Linux?

The PS3 has an nvidia graphics chip of the G70 family (called RSX), and a [[project|http://wiki.ps2dev.org/ps3:rsx]] has been started for PS3 specifically to take advantage of that chip. A way to use the chip for 3D has been found with some firmware versions, however Sony quickly closed it down, and seems willing to close any way to properly use the RSX under Linux. Therefore, we do not advise getting a PS3 for the purpose of using the RSX under Linux. 


### Will you support Tegra chips?

Essentially there are two types of Tegra systems:

1. Tegra 1-4i: They have a custom made GPU which is independent from the "normal" Nvidia GPU line. See The [[grate-project|https://github.com/grate-driver]] for hardware acceleration support.
2. Tegra K1 and newer: they use the "normal" Nvidia GPU for acceleration. They are supported on a best effort basis. Supported Chips are K1, X1 and X2.

### Will you support Xv?

We do support Xv. Nouveau uses the "nv" code to support [[Xv|http://en.wikipedia.org/wiki/X_video_extension]], which was improved by [[ArthurHuillet|ArthurHuillet]] and it provides two texture adapters which use the 3D engine of the NV30 and NV40 cards. They provide bilinear and high-quality bicubic filtering. 


### Will you support XvMC/VDPAU (hardware accelerated video decoding)?

[[XvMC (X-Video Motion Compensation)|http://nouveau.freedesktop.org/wiki/XvMC]] is supported on some chips ([[nv40-nv96, nva0|CodeNames]]) through Mesa's XvMC state tracker. [[VDPAU]] is supported on [[NV84+|CodeNames]] chips. See [[VideoAcceleration]] for more details.


### How do I install the 3D driver?

See [[MesaDrivers|MesaDrivers]]. 


### How do I set up a desktop for multiple cards or monitors?

See [[MultiMonitorDesktop|MultiMonitorDesktop]]. 


### The fan is really LOUD all the time. How do I make it more quiet?

On many cards, the fan is temperature controlled already, but obviously in your case it is stuck at the maximum speed. We need some help from you to fix it, but we do not know yet what you could do for us, exactly. Please, check this FAQ item every once in a while, we will tell you here what to do, when we figure it out. In the mean time, if you are interested, you can read [[PowerManagement|PowerManagement]] and/or contact mupuf on the #nouveau OFTC IRC channel.

Your help is needed to fix this issue. 


## Legal questions


### Do you violate NVidia’s EULA with renouveau?

No. We don't  touch NVidia’s binary blob at all, we just observe what the driver changes in memory. All config data we have, is exposed in some /proc or /dev files. And running OpenGL is the main reason why you would use the driver. That is no violation. 


### What license does Nouveau use?

Nouveau is made of 3 components: DDX (2D driver), DRI (3D driver) and DRM (kernel component). The DDX and DRI use the MIT license, the DRM uses a dual MIT/GPL license. REnouveau is under GPL. 
