This page describes games from the viewpoint of the driver, i.e. what functionality they use. In particular, how they sumbit vertices and what extensions are used 

* Wine : prefered vertex path is VBOs 
* Unreal tournament: unknown 
* Quake 3 engine: [[http://www.gamers.org/dEngine/quake3/johnc_glopt.html|http://www.gamers.org/dEngine/quake3/johnc_glopt.html]] 
GL_S3_s3tc  GL_EXT_texture_env_add  GL_ARB_multitexture  GL_EXT_compiled_vertex_array  

* Enemy territory: 
GL_S3_s3tc GL_EXT_texture_env_add GL_ARB_multitexture GL_EXT_compiled_vertex_array GL_NV_fog_distance GL_EXT_texture_filter_anisotropic GLX_SGI_swap_control GLX_SGI_video_sync 

* Doom 3 engine: differs for each renderer (there are 4: ARB, NV20, R200 and ARB2). Here is a paste of the detected OpenGL extensions (will need to sort it later to figure out what renderer requires what extensions): 
GL_ARB_multitexture GL_ARB_texture_env_combine GL_ARB_texture_cube_map GL_ARB_texture_env_dot3 GL_ARB_texture_env_add GL_ARB_texture_compression GL_EXT_texture_lod GL_1.4_texture_lod_bias GL_EXT_shared_texture_palette GL_EXT_texture3D GL_EXT_stencil_wrap GL_NV_register_combiners GL_EXT_stencil_two_side GL_ATI_separate_stencil GL_ATI_fragment_shader GL_ATI_text_fragment_shader GL_ARB_vertex_buffer_object GL_ARB_vertex_program GL_ARB_fragment_program GL_ARB_texture_non_power_of_two GL_EXT_texture_compression_s3tc GL_EXT_texture_filter_anisotropic 

* darkplaces (quake1 based) : vertex arrays, glDrawElements + locked arrays (very similar to quake3), attributes used are vertex (3 floats)/color (4 floats)/multitextures (2 or 3 floats) 